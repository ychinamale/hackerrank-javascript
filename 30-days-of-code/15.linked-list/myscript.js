process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});
function readLine() {
    return input_stdin_array[input_currentline++];
}
function Node(data){
    this.data=data;
    this.next=null;
}
function Solution(){

	this.insert=function(head,data){
        //complete this method
        //console.log(`Inserting ${data}`)

        let newNode = new Node(data)
        //console.log(`created new node`)

        if (head == null){
            head = newNode
            //console.log(`Updated head node to ${head.data}`)
            return head
        }

        if (head != null){
            //console.log(`head already exists`)
            thisNode = head
            
            if(thisNode.next == null){
                //console.log(`this node ${thisNode.data} doesnt have a next. Creating...`)
                thisNode.next = newNode
                return head
            } else {
                //console.log(`this node ${thisNode.data} has a next. i.e. ${thisNode.next.data}`)
                this.insert(thisNode.next, data)
            }
        }

        return head
    }

	this.display=function(head){
        var start=head;
            while(start){
                process.stdout.write(start.data+" ");
                start=start.next;
            }
    };
}
function main(){
    var T=parseInt(readLine());
    var head=null;
    var mylist=new Solution();
    for(i=0;i<T;i++){
        var data=parseInt(readLine());
        head=mylist.insert(head,data);
    }
    mylist.display(head);
}